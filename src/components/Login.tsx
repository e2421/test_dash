import React, { Component, useState } from "react";
import { useForm } from 'react-hook-form'; 
import { BrowserRouter as Router, Route, Routes, Link, useNavigate } from 'react-router-dom';
import axios from 'axios';
import path from '../env';

var CryptoJS = require("crypto-js");

const Login = ( ) => { 
  const navigate = useNavigate();

  const [load, setLoad] = useState(false);

  const encrypt = (string:string) => {
    return CryptoJS.AES.encrypt(JSON.stringify(string), 'ejima').toString();
  }

  const {
    register,handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = (data: any ) => {
    setLoad(true);

    const headers = {
      'Content-Type': 'application/json',  
    }
    axios.post(path.online+`/admins/login`,data , {headers: headers}) //
      .then(res => {
        setLoad(false) 
        localStorage.setItem('userRole', encrypt(res.data.admin.role) )
        localStorage.setItem('userData',encrypt(JSON.stringify(res.data.admin)))
        localStorage.setItem('userStatus', encrypt(res.data.admin.active))
        localStorage.setItem('token',res.data.token)
        navigate('/');
      }) 
  }

  return (  
    <div className='container-fluid h-100 d-flex justify-content-center align-items-center' 
      style={{backgroundColor: '#EB4876', height: '100%', }}>

      {load === true ? 
        <div className="spinner-border text-light" role="status">
          <span className="sr-only">Loading...</span>
        </div>

        : 
      
        <div className="card col-4 mt-5 mb-5" style={{borderRadius:10, }}>
          <div className="card-body">    
            <div className='text-center'><strong><h4>Welcome Back !</h4></strong></div>
            <form className='p-3 d-flex flex-column align-items-start'  onSubmit={handleSubmit(onSubmit)}>
              <div className="form-group d-flex flex-column align-items-start w-100 mt-2">
                <label> <strong>Email</strong> </label>
                <input type="email" className="form-control" placeholder="Email" {...register('email', { required: true })} /> 
                {errors.email && <p className='text-danger'>Email is required.</p>}
              </div>

              <div className="form-group d-flex flex-column align-items-start w-100 mt-2">
                <label> <strong>Password</strong> </label>
                <input type="password" className="form-control" placeholder="Password" {...register('password', { required: true })} /> 
                {errors.password && <p className='text-danger'>Password is required.</p>}
              </div> 

              <div className="d-flex justify-content-center  w-100">
                <small className="" > <Link to={"/forgotpass"} className="nav-link">Forget password ?</Link> </small>
              </div>
              
              <button type="submit" className="btn btn-light w-100 mt-3">Login</button>
              <Link className='w-100' to="/register"><button type="submit" className="btn btn-light w-100 mt-3">Create an Account</button></Link>
            </form>
          </div>
        </div> 
      }  
    </div> 
  );
}

export default Login