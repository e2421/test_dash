import React, { Component, useState } from "react";
import { useForm } from 'react-hook-form'; 
import { BrowserRouter as Router, Route, Routes, Link, useNavigate } from 'react-router-dom';
import axios from 'axios';
import path from '../env';

var CryptoJS = require("crypto-js");

const Forgotpass = ( ) => { 
  const navigate = useNavigate();

  const [load, setLoad] = useState(false);
  const [step2, setStep2] = useState(false);
  const [id, setId] = useState(" ");
  const [code, setCode] = useState(" ");

  const {
    register,handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmitEmail = (data: any ) => { 
    setLoad(true)

    //send verif mail
    let datas = {
      'email': data.email, 
    }
    const headers = {
      'Content-Type': 'application/json',
    }
    //send the mail to user 
    axios.post(path.online+`/admins/sendResetPMail`,datas, {headers: headers})
      .then(res => { 
        setLoad(false) 
        if (res.data.message === "success"){
          setStep2(true)
          setId(res.data.id)
          setCode(res.data.code)
        }
        //navigate('/login'); 
      }) 
  }

  const onSubmitNewPass = (data: any ) => { 
    setLoad(true)

    if( data.password === data.confpassword && data.code === code ){
      //update new password
      const headers = {
        'Content-Type': 'application/json',  
      }
      axios.put(path.online+`/admins/`+id+'/NoLog', {password: data.password} , {headers: headers})
        .then(async res => { 
          setStep2(false) 
          //navigate('/login'); 
        })
    }
  }

  return (  
    <div className='container-fluid h-100 d-flex justify-content-center align-items-center' 
      style={{backgroundColor: '#EB4876', height: '100%', }}>

      {load === true ? 
        <div className="spinner-border text-light" role="status">
          <span className="sr-only">Loading...</span>
        </div>

        : 
          // get email
          <div className="card col-4 mt-5 mb-5" style={{borderRadius:10, }}>
            <div className="card-body">    
              {step2 === false ? 
                // get email
                <form className='p-3 d-flex flex-column align-items-start'  onSubmit={handleSubmit(onSubmitEmail)}>
                  <div className='text-center mx-auto'><strong><h4>Enter your email to reset the password</h4></strong></div>
                  
                  <div className="form-group d-flex flex-column align-items-start w-100 mt-2">
                    <label> <strong>Email</strong> </label>
                    <input type="email" className="form-control" placeholder="Email" {...register('email', { required: true })} /> 
                    {errors.email && <p className='text-danger'>Email is required.</p>}
                  </div> 

                  <button type="submit" className="btn btn-light w-100 mt-3">Submit</button>
                </form>
              : 
              // get new password
              <form className='p-3 d-flex flex-column align-items-start'  onSubmit={handleSubmit(onSubmitNewPass)}>
                <div className='text-center mx-auto'><strong><h4>Reset the password</h4></strong></div>

                <div className="form-group d-flex flex-column align-items-start w-100 mt-2">
                  <label> <strong>Code</strong> </label>
                  <input type="text" className="form-control" placeholder="Code" {...register('code', { required: true })} /> 
                  {errors.code && <p className='text-danger'>Code is required.</p>}
                </div>

                <div className="form-group d-flex flex-column align-items-start w-100 mt-2">
                  <label> <strong>New Password</strong> </label>
                  <input type="password" className="form-control" placeholder="Password" {...register('password', { required: true, maxLength: 8  })} /> 
                  {errors.password && <p className='text-danger'>Password is required.</p> && <p className='text-danger'>At least 8 characters.</p>}
                </div> 

                <div className="form-group d-flex flex-column align-items-start w-100 mt-2">
                  <label> <strong>Confirm New Password</strong> </label>
                  <input type="password" className="form-control" placeholder="Confirm Password" {...register('confpassword', { required: true, maxLength: 8 })} /> 
                  {errors.confpassword && <p className='text-danger'>Confirm Password is required.</p> && <p className='text-danger'>At least 8 characters.</p>}
                </div> 
                
                <button type="submit" className="btn btn-light w-100 mt-3">Submit</button> 
              </form>
            }
            </div>
          </div> 
        
      }  
    </div> 
  );
}

export default Forgotpass