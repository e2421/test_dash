import React, { useState } from "react";
import { useForm } from 'react-hook-form';
import { serialize } from 'object-to-formdata';
import { BrowserRouter as Router, Route, Routes, Link, useNavigate} from 'react-router-dom';
import axios from 'axios';
import path from '../env';

const Register = ( ) => { 
  const navigate = useNavigate();
  const [load, setLoad] = useState(false); 
  const [error, setError] = useState(false); 
  
  const {
    register,handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = async (data: any ) => { 
    let infos = {
      'email':'',
      'code': '',
    } 
    setLoad(true)
    let picture = data.picture[0];
    delete data.picture 
    let object = {
      'objectdata':JSON.stringify(data), 
      'fileprop': 'pictures',
      'filedata0': picture,
      'filenumber': 1,
    }
    const formData = serialize( object, );
    
    const headers0 = {
      'Content-Type': 'multipart/form-data',
    }
    const headers1 = {
      'Content-Type': 'application/json',  
    }
    //register user
    axios.post(path.online+`/admins/register-file`,formData , {headers: headers0})
      .then(res => {
        console.log(res.status)
        if(res.status === 408){
          setError(true)
        }
        //send verif mail
        let datas = {
          'email': res.data.user.email,
          'code': res.data.user.code,
          'fullname': res.data.user.firstname + " " +res.data.user.lastname 
        }
        //send the mail to user 
        axios.post(path.online+`/admins/sendCheckMail`,datas, {headers: headers1})
          .then(res => {
            if(res.status === 408){
              setError(true)
            }
             navigate('/login'); 
          }) 
      })  
  }

  return (
    <div className='container-fluid h-100 d-flex justify-content-center align-items-center' 
      style={{backgroundColor: '#1A8DF6', height: '100%', }}>
      
      {load === true ? 
        <div className="spinner-border text-light" role="status">
          <span className="sr-only">Loading...</span>
        </div>

        : 

        <div className="card col-4 mt-5 mb-5" style={{borderRadius:10, }}> 
          <div className="card-body">    
            <div className='text-center'><strong><h4>Sign Up</h4></strong></div> 
            <form className='p-3 d-flex flex-column align-items-start'  onSubmit={handleSubmit(onSubmit)}>
              <div className="form-group d-flex flex-column align-items-start w-100 mt-2">
                <label> <strong>Firstname</strong> </label>
                <input type="text" className="form-control" placeholder="Firstname" {...register('firstname', { required: true })} /> 
                {errors.firstname && <p className='text-danger'>Firstname is required.</p>}
              </div>

              <div className="form-group d-flex flex-column align-items-start w-100 mt-2">
                <label> <strong>Lastname</strong> </label>
                <input type="text" className="form-control" placeholder="Last Name" {...register('lastname', { required: true })} /> 
                {errors.lastname && <p className='text-danger'>Lastname is required.</p>}
              </div>

              <div className="form-group d-flex flex-column align-items-start w-100 mt-2">
                <label> <strong>Email</strong> </label>
                <input type="email" className="form-control" placeholder="Email" {...register('email', { required: true })} /> 
                {errors.email && <p className='text-danger'>Email is required.</p>}
              </div>

              <div className="form-group d-flex flex-column align-items-start w-100 mt-2">
                <label> <strong>Password</strong> </label>
                <input type="password" className="form-control" placeholder="Password" {...register('password', { required: true, maxLength: 8 })} /> 
                {errors.password && <p className='text-danger'>Password is required.</p> && <p className='text-danger'>At least 8 characters.</p>}
              </div>,

              <div className="form-group d-flex flex-column align-items-start w-100 mt-2">
                <label> <strong>Picture</strong> </label>
                <input type="file" className="form-control" {...register('picture', {/*{ required: true }*/})} />      
                {/* {errors.picture && <p className='text-danger'>The picture is required.</p>}      */}
              </div>
              
              <button type="submit" className="btn btn-primary w-100 mt-3">Sign Up</button>

              <div className="d-flex justify-content-end  w-100">
                <Link className='w-100' to="/login"><small className="mt-3">Already registered, sign in ?</small></Link>
              </div>

            </form> 
          </div>
        </div>
      }

      {error === true ? 
        <div className="text-light"> <h3> Timeout exceeded, try later !</h3> </div>
        :
        <div></div>
      } 
      
    </div> 
  );
}

export default Register