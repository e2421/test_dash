export default {
    online: "https://ejima-test-api.herokuapp.com/api",
    local: "http://0.0.0.0:4000/api",
}