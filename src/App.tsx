import React, { useState, Component, useEffect } from 'react';
import { BrowserRouter as Router, Route, Routes, Link } from 'react-router-dom';
import './App.css';

import Register from './components/Register'; 
import Login from './components/Login';
import Home from './components/Home';
import Forgotpass from './components/Forgotpass';


const App = ( ) =>  { 
  const makeAPICall = async () => {
    try { 

      const response = await fetch('https://ejima-test-api.herokuapp.com/cors', {mode:'cors'});
      const data = await response.json();
      console.log({ data })
    
    }
    catch (e) {
      console.log(e)
    }
  }

  useEffect(() => {
    // makeAPICall();
  }, [])

  // render() {
    return ( 
      <Router>
        <div className="App" style={{height: '100vh', backgroundColor: '#EBECF0'}}>
          {/* <ul className="App-header">
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/register">R</Link>
            </li>
            <li>
              <Link to="/login">L</Link>
            </li>
          </ul> */}
  
          <Routes>
            <Route  path='/' element={< Home />}></Route>
            <Route  path='/register' element={< Register />}></Route>
            <Route  path='/login' element={< Login />}></Route>
            <Route  path='/forgotpass' element={< Forgotpass />}></Route> 
          </Routes>
        </div>
      </Router>
             
    );
  // }

  
}

export default App;
